# Convert markdown to the markup format used in jira

The
[markup](https://jira.atlassian.com/secure/WikiRendererHelpAction.jspa?section=all)
language used in jira is not very common. In contrast,
[Markdown](https://daringfireball.net/projects/markdown/) is widely used
and editing it is supported by a lot of editors. I like to keep my
documentation in markdown, but when I need to paste them into jira forms,
this doesn't work very well. This is where this small command-line tool
helps. It uses the great
[pulldown-cmark](https://crates.io/crates/pulldown-cmark/) library for
reading the [CommonMark flaver of Markdown](https://commonmark.org/) either
from stdin or from a file, and converts it to the format used in jira,
either to stdout or to a file.

## Installation

First you need to have a working `rustc` + `cargo` installation. This can
be achieved by either installing the packages available for your
distribution, or through https://rustup.rs/. Then you can can install
`md2jira` by running `cargo install -f md2jira`. The `-f` flag forces
previous versions to be overwritten.

## Example usage

```
user@host:~/Projects/md2jira$ cat example.md
# Markdown and Jira

I typically write notes in [Markdown](https://daringfireball.net/projects/markdown/).

---

## md2jira can convert:

* Lists
    1. Nested
    2. Ordered
    3. Unordered
* Tables
* Headings
* *Strong*
* **Emphasis**
* ~~Strikethrough~~
* Horizontal rules
* etc…
user@host:~/Projects/md2jira$ md2jira -i example.md
h1. Markdown and Jira

I typically write notes in [Markdown|https://daringfireball.net/projects/markdown/].

----

h2. md2jira can convert:

* Lists
*# Nested
*# Ordered
*# Unordered
* Tables
* Headings
* _Strong_
* *Emphasis*
* -Strikethrough-
* Horizontal rules
* etc…
user@host:~/Projects/md2jira$ 
```

## Alternatives

* [pandoc](https://pandoc.org/) can convert from and to multiple formats,
  and it has support for several markdown flavors as well as the jira
  markup format. Based on what I have experienced from using pandoc, their
  support for both formats is probably superior to what I implemented in
  `md2jira`. Bonus points for conversion into both directions. A conversion
  can be achieved with a command like this:
  ```
  pandoc --from commonmark --to jira example.md
  ```
