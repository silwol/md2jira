use anyhow::{anyhow, bail, Result};
use pulldown_cmark::{CodeBlockKind, Event, Options, Parser, Tag};
use std::fs::File;
use std::io::{stdin, stdout, BufRead, BufReader, BufWriter, Write};
use std::path::{Path, PathBuf};
use structopt::StructOpt;

fn main() -> Result<()> {
    let args = Args::from_args();

    let stdin = stdin();
    let stdout = stdout();

    let inputpath =
        args.input.unwrap_or_else(|| Path::new("-").to_path_buf());
    let mut input: Box<dyn BufRead> = match inputpath.to_str() {
        Some("-") | None => Box::new(stdin.lock()),
        Some(_) => Box::new(BufReader::new(
            File::open(&inputpath).map_err(|e| {
                anyhow!(
                    "Can't open input file {:?}: {}",
                    inputpath.to_string_lossy(),
                    e
                )
            })?,
        )),
    };

    let outputpath =
        args.output.unwrap_or_else(|| Path::new("-").to_path_buf());
    let mut output: Box<dyn Write> = match outputpath.to_str() {
        Some("-") | None => Box::new(stdout.lock()),
        Some(_) => Box::new(BufWriter::new(
            File::create(&outputpath).map_err(|e| {
                anyhow!(
                    "Can't open output file {:?} for writing: {}",
                    inputpath.to_string_lossy(),
                    e
                )
            })?,
        )),
    };

    // I'd rather prefer to parse in a streaming manner, but it seems
    // `pulldown-cmark::Parser` can only be created with an `&str`. In
    // practise this shouldn't be too much of a problem, since the
    // documents we convert are usually not too large.
    let mut buffer = String::new();
    input
        .read_to_string(&mut buffer)
        .map_err(|e| anyhow!("Error reading input: {}", e))?;

    let parser = Parser::new_ext(&buffer, parser_options());

    write_jira(&mut output, parser)
}

fn parser_options() -> Options {
    let mut options = Options::empty();
    options.insert(Options::ENABLE_TABLES);
    options.insert(Options::ENABLE_STRIKETHROUGH);
    options
}

#[derive(Debug, StructOpt)]
struct Args {
    /// The input. If a file path is given, this is used as the input file.
    /// If this is either not present or a plain '-', then the input data
    /// is read from stdin.
    #[structopt(short, long, name = "INPUT")]
    input: Option<PathBuf>,

    /// The output. If a file path is given, this is used as the output file.
    /// If the file already exists, it will be overwritten.
    /// If this is either not present or a plain '-', then the output data
    /// is written to stdout.
    #[structopt(short, long, name = "OUTPUT")]
    output: Option<PathBuf>,
}

fn write_jira<'a, I, W>(writer: &'a mut W, iter: I) -> Result<()>
where
    I: Iterator<Item = Event<'a>>,
    W: Write,
{
    JiraWriter::new(iter, writer).run()
}

#[derive(Debug)]
enum ListLevel {
    Ordered,
    Unordered,
}

impl ToString for ListLevel {
    fn to_string(&self) -> String {
        match self {
            ListLevel::Ordered => "#".to_string(),
            ListLevel::Unordered => "*".to_string(),
        }
    }
}

struct JiraWriter<'a, I, W> {
    iter: I,
    writer: &'a mut W,
    last_written: Option<char>,
    second_last_written: Option<char>,
    table_head: bool,
    write_text_raw: bool,
    list_stack: Vec<ListLevel>,
}

impl<'a, I, W> JiraWriter<'a, I, W>
where
    I: Iterator<Item = Event<'a>>,
    W: Write,
{
    fn new(iter: I, writer: &'a mut W) -> Self {
        Self {
            iter,
            writer,
            last_written: None,
            second_last_written: None,
            table_head: false,
            write_text_raw: false,
            list_stack: Vec::new(),
        }
    }

    fn run(mut self) -> Result<()> {
        while let Some(event) = self.iter.next() {
            match event {
                Event::Start(tag) => {
                    self.start_tag(tag)?;
                }
                Event::End(tag) => {
                    self.end_tag(tag)?;
                }
                Event::Text(text) => {
                    self.write(&text)?;
                }
                Event::Code(text) => {
                    self.write("{{")?;
                    self.write(&text)?;
                    self.write("}}")?;
                }
                Event::Html(_html) => {
                    eprintln!("Embedding HTML is not supported by Jira");
                }
                Event::SoftBreak => {
                    match self.last_written {
                        None => {}
                        Some(c) if c != '\n' => {
                            if self.write_text_raw {
                                self.write("\n")?;
                            } else {
                                self.write(" ")?;
                            }
                        }
                        _ => {}
                    };
                }
                Event::HardBreak => {
                    self.write("\n")?;
                }
                Event::Rule => {
                    self.write_separation_line()?;
                    self.write("----")?;
                }
                Event::FootnoteReference(_name) => {
                    eprintln!("Footnote reference not supported by Jira");
                }
                Event::TaskListMarker(_active) => {
                    eprintln!("Task list marker not supported by Jira");
                }
            }
        }
        self.write("\n")?;
        Ok(())
    }

    fn start_tag(&mut self, tag: Tag<'a>) -> Result<()> {
        match tag {
            Tag::Paragraph => {
                if !self.write_text_raw {
                    self.write_separation_line()?;
                }
            }
            Tag::Heading(level) => match level {
                nr if nr >= 1 && nr <= 6 => {
                    self.write_separation_line()?;
                    self.write(&format!("h{}. ", level))?;
                }
                nr => {
                    bail!("Heading level {} not supported, allowed are level 1 to 6", nr);
                }
            },
            Tag::Table(_alignments) => {
                self.write_separation_line()?;
            }
            Tag::TableHead => {
                self.table_head = true;
            }
            Tag::TableRow => {
                self.write("\n")?;
            }
            Tag::TableCell => {
                if self.table_head {
                    self.write("||")?;
                } else {
                    self.write("|")?;
                }
            }
            Tag::BlockQuote => {
                self.write_separation_line()?;
                self.write("{quote}\n")?;
                self.write_text_raw = true;
            }
            Tag::CodeBlock(info) => {
                self.write_separation_line()?;
                match info {
                    CodeBlockKind::Indented => {
                        self.write("{code}\n")?;
                    }
                    CodeBlockKind::Fenced(fence) => {
                        self.write(&format!("{{code:{}}}\n", fence))?;
                    }
                }
                self.write_text_raw = true;
            }
            Tag::List(details) => {
                if self.list_stack.is_empty() {
                    self.write_separation_line()?;
                }
                if details.is_some() {
                    self.list_stack.push(ListLevel::Ordered);
                } else {
                    self.list_stack.push(ListLevel::Unordered);
                }
            }
            Tag::Item => {
                match self.last_written {
                    Some('\n') | None => {}
                    Some(_) => self.write("\n")?,
                };
                let s = self
                    .list_stack
                    .iter()
                    .map(ToString::to_string)
                    .collect::<Vec<String>>()
                    .join("");
                self.write(&format!("{} ", s))?;
            }
            Tag::Emphasis => {
                self.write("_")?;
            }
            Tag::Strong => {
                self.write("*")?;
            }
            Tag::Strikethrough => {
                self.write("-")?;
            }
            Tag::Link(_link_type, _dest, _title) => {
                self.write("[")?;
            }
            Tag::Image(_link_type, _dest, _title) => {
                eprintln!("No image support implemented in this tool");
            }
            Tag::FootnoteDefinition(_name) => {
                eprintln!("No footnote definition support");
            }
        }
        Ok(())
    }

    fn end_tag(&mut self, tag: Tag<'a>) -> Result<()> {
        use pulldown_cmark::LinkType;
        match tag {
            Tag::Paragraph => {}
            Tag::Heading(_level) => {}
            Tag::Table(_alignments) => {}
            Tag::TableHead => {
                self.write("||")?;
                self.table_head = false;
            }
            Tag::TableRow => {
                self.write("|")?;
            }
            Tag::TableCell => {}
            Tag::BlockQuote => {
                match self.last_written {
                    Some('\n') => {}
                    _ => {
                        self.write("\n")?;
                    }
                };
                self.write("{quote}")?;
                self.write_text_raw = false;
            }
            Tag::CodeBlock(_info) => {
                match self.last_written {
                    Some('\n') => {}
                    _ => {
                        self.write("\n")?;
                    }
                };
                self.write("{code}")?;
                self.write_text_raw = false;
            }
            Tag::List(_) => {
                self.list_stack.pop();
            }
            Tag::Item => {}
            Tag::Emphasis => {
                self.write("_")?;
            }
            Tag::Strong => {
                self.write("*")?;
            }
            Tag::Strikethrough => {
                self.write("-")?;
            }
            Tag::Link(LinkType::Email, dest, _title) => {
                self.write(&format!("|mailto:{}]", dest))?;
            }
            Tag::Link(_link_type, dest, _title) => {
                self.write(&format!("|{}]", dest))?;
            }
            Tag::Image(_link_type, _dest, _title) => {}
            Tag::FootnoteDefinition(_name) => {}
        }
        Ok(())
    }

    #[inline]
    fn write_separation_line(&mut self) -> Result<()> {
        match (self.second_last_written, self.last_written) {
            (Some('\n'), Some('\n')) => {}
            (Some(_), Some('\n')) => {
                self.second_last_written = self.last_written;
                self.last_written = Some('\n');
                self.writer.write_str("\n")?;
            }
            (_, Some(_)) => {
                self.second_last_written = Some('\n');
                self.last_written = Some('\n');
                self.writer.write_str("\n")?;
                self.writer.write_str("\n")?;
            }
            (_, None) => {}
        }
        Ok(())
    }

    #[inline]
    fn write(&mut self, s: &str) -> Result<()> {
        self.writer.write_str(s)?;
        if s.len() >= 2 {
            self.second_last_written = s.chars().nth_back(1);
            self.last_written = s.chars().nth_back(0);
        } else if s.len() == 1 {
            self.second_last_written = self.last_written;
            self.last_written = s.chars().last();
        }
        Ok(())
    }
}

trait StrWrite {
    fn write_str(&mut self, s: &str) -> Result<()>;
}

impl<W> StrWrite for W
where
    W: Write,
{
    #[inline]
    fn write_str(&mut self, s: &str) -> Result<()> {
        Ok(self.write_all(s.as_bytes())?)
    }
}

#[cfg(test)]
mod tests {
    use super::{parser_options, write_jira, Parser, Result};

    fn convert(input: &str) -> Result<String> {
        let parser = Parser::new_ext(&input, parser_options());
        let mut output: Vec<u8> = Vec::new();

        write_jira(&mut output, parser)?;
        let mut s = String::new();
        use std::io::Read;
        output.as_slice().read_to_string(&mut s)?;
        Ok(s)
    }

    #[test]
    fn test_text() {
        let md = "Hello World!";
        let expected = "Hello World!\n";
        assert_eq!(convert(md).unwrap(), expected);
    }

    #[test]
    fn test_heading() {
        let md = "\
        # My Document\n\
        Some text\n\
        \n\
        ## Level 2 heading\n\
        Some more text";

        let expected = "\
        h1. My Document\n\
        \n\
        Some text\n\
        \n\
        h2. Level 2 heading\n\
        \n\
        Some more text\n";
        assert_eq!(convert(md).unwrap(), expected);
    }

    #[test]
    fn test_code() {
        let md = "Text with some `code` inside";
        let expected = "Text with some {{code}} inside\n";
        assert_eq!(convert(md).unwrap(), expected);
    }

    #[test]
    fn test_softbreak() {
        let md = "\
        Text\n\
        with softbreak";
        let expected = "\
        Text with softbreak\n";
        assert_eq!(convert(md).unwrap(), expected);
    }

    #[test]
    fn test_hardbreak() {
        let md = "\
        Text\\\n\
        with hardbreak";
        let expected = "\
        Text\nwith hardbreak\n";
        assert_eq!(convert(md).unwrap(), expected);
    }

    #[test]
    fn test_rule() {
        let md = "\
        Some text\n\
        \n\
        ---\n\
        with\n\
        ___\n\
        a few\n\
        ***\n\
        horizontal rules";
        let expected = "\
        Some text\n\
        \n\
        ----\n\
        \n\
        with\n\
        \n\
        ----\n\
        \n\
        a few\n\
        \n\
        ----\n\
        \n\
        horizontal rules\n";
        assert_eq!(convert(md).unwrap(), expected);
    }

    #[test]
    fn test_table() {
        let md = "\
        This is a table:\n\
        \n\
        | Order | Mountain Name | Height |\n\
        |-------|---------------|--------|\n\
        | 1.    | Großglockner  | 3798 m |\n\
        | 2.    | Wildspitze    | 3768 m |\n\
        | 3.    | Weißkugel     | 3738 m |\n\
        ";
        let expected = "\
        This is a table:\n\
        \n\
        ||Order||Mountain Name||Height||\n\
        |1.|Großglockner|3798 m|\n\
        |2.|Wildspitze|3768 m|\n\
        |3.|Weißkugel|3738 m|\n";
        assert_eq!(convert(md).unwrap(), expected);
    }

    #[test]
    fn test_blockquote() {
        let md = "\
        A block quote:\n\
        \n\
        > Wir schaffen das!\n\
        \n\
        Bob the Builder\n\
        \n\
        Another one:\n\
        > So be prepared,\n\
        > be enthusiastic,\n\
        > and leave your bullshit attitude\n\
        > and baggage at the door\n\
        > because we don't need it!";
        let expected = "\
        A block quote:\n\
        \n\
        {quote}\n\
        Wir schaffen das!\n\
        {quote}\n\
        \n\
        Bob the Builder\n\
        \n\
        Another one:\n\
        \n\
        {quote}\n\
        So be prepared,\n\
        be enthusiastic,\n\
        and leave your bullshit attitude\n\
        and baggage at the door\n\
        because we don\'t need it!\n\
        {quote}\n";
        assert_eq!(convert(md).unwrap(), expected);
    }

    #[test]
    fn test_codeblock_indented() {
        let md = "\
        Some indented code:\n\
        \n\
        \u{20}   fn hello() {\n\
        \u{20}       println!(\"Hello!\");\n\
        \u{20}   }";
        let expected = "\
        Some indented code:\n\
        \n\
        {code}\n\
        fn hello() {\n\
        \u{20}   println!(\"Hello!\");\n\
        }\n\
        {code}\n";
        assert_eq!(convert(md).unwrap(), expected);
    }

    #[test]
    fn test_codeblock_fenced() {
        let md = "\
        Some fenced code:\n\
        \n\
        ```rust\n\
        fn hello() {\n\
        \u{20}   println!(\"Hello!\");\n\
        }\n\
        ```";
        let expected = "\
        Some fenced code:\n\
        \n\
        {code:rust}\n\
        fn hello() {\n\
        \u{20}   println!(\"Hello!\");\n\
        }\n\
        {code}\n";
        assert_eq!(convert(md).unwrap(), expected);
    }

    #[test]
    fn test_list() {
        let md = "\
        A nested list:\n\
        \n\
        * Item 1\n\
        * Item 2\n\
        \u{20}   1. Hello\n\
        \u{20}   2. World\n\
        * Item 3\n\
        \u{20}   * Item 3.1\n\
        \u{20}   * Item 3.2";
        let expected = "\
        A nested list:\n\
        \n\
        * Item 1\n\
        * Item 2\n\
        *# Hello\n\
        *# World\n\
        * Item 3\n\
        ** Item 3.1\n\
        ** Item 3.2\n";
        assert_eq!(convert(md).unwrap(), expected);
    }

    #[test]
    fn test_emphasis() {
        let md = "Some text with *emphasis* in it";
        let expected = "Some text with _emphasis_ in it\n";
        assert_eq!(convert(md).unwrap(), expected);
    }

    #[test]
    fn test_strong() {
        let md = "Some text with **strong** in it";
        let expected = "Some text with *strong* in it\n";
        assert_eq!(convert(md).unwrap(), expected);
    }

    #[test]
    fn test_strikethrough() {
        let md = "Some text with ~~strikethrough~~ in it";
        let expected = "Some text with -strikethrough- in it\n";
        assert_eq!(convert(md).unwrap(), expected);
    }

    #[test]
    fn test_email_link() {
        let md = "An email link: <me@example.com>";
        let expected =
            "An email link: [me@example.com|mailto:me@example.com]\n";
        assert_eq!(convert(md).unwrap(), expected);
    }

    #[test]
    fn test_link() {
        let md = "Two links: [https://example.com/] or [Example](https://example.com/).";
        let expected =
            "Two links: [https://example.com/] or [Example|https://example.com/].\n";
        assert_eq!(convert(md).unwrap(), expected);
    }
}
